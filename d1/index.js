console.log("Hello World");

//It will comment parts of the code that gets ignored by the language

// [Section] Syntax, statements
	// Statements in programming are instructions that we tell the computer to perform
	//JS statements usually end with semi-colon (;)
	//Semi-colons are not required in JS but we will use it to help us train to locate where statement ends.
	//A syntax in programming, it is the set of rules taht describes how statements must be constructed
	// All lines/blocks of code should be written in specific manner to work

//[Section] Variable
	//Variables are used to contain data
	//Any information that is used by an application is stored in what we call the memory
	
	//Declaring Variables
	// Syntax:
		// let/const variableName;
	
	let myVariable = "Ada Lovelace";
	const constVariable = "John Doe";
	//console.log() is useful for printing values of variables or certain results of code into the browser's console
	console.log(myVariable);
	/*
		Guides in writing variables:
			1. Use the let keyword followed by the variable name of your choice and use the assignment opereator (=) to assign
			2. Variable names should start with lowercase character, use camelCasing for mult words.
			3. For constant variables, use the 'const' keyword
				Note: If we use const keyword in declaring a variable, we cannot change the value of its variable
			4. Variable names should be indicative of the value being stored to avoid confusion.
	*/

	//Declare and initialize
		// Initializing Variables - the instance when a variable is given it's first value
	let productName = "desktop computer";
	console.log(productName);

	productName = "personal computer";
	console.log(productName);

	const name="John";
	console.log(name);
	// name = "Jan";
	// console.log(name);

	lastName = "Mortel";

	var lastName;
	console.log(lastName);


	let outerVar = "Hello"
	{
		let innerVar = "Hello Again"
		console.log(innerVar);
	}

	console.log(outerVar);

	//Multiple variable declarations

	let productCode = "DC017", productBrand = "Dell"
	console.log(productCode);
	console.log(productBrand);


//[Section] Data Types
	//String
	/*
		Strings are a series of characters that create a word, phrase, sentence or anything created with text.

		Strings in JS can be written using either single('') or double("") quote
	*/
let country = "Philippines"
let province = 'Metro Manila'
console.log(country);
console.log(province);
let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = "I live in the "+country;	
console.log(greeting)

let message = 'John\'s employees cnasdf\\';
console.log(message);

//Numbers
	let headCount = 27;
	console.log(headCount);
	console.log(typeof headCount);

	let grade= 98.7;
	console.log(grade);
	console.log(typeof grade);

//Exponential Notation
	let planetDistance = 2e10;
	console.log(planetDistance);
	console.log(typeof planetDistance);

//Combining text and strings
	console.log("John's grade last quarter is "+grade);
	console.log(typeof "John's grade last quarter is "+typeof grade);

//Boolean
	/*
		Boolean values are normally used to store values relating to the state of certain things
		Value can only be true or false
	*/
let isMarried = false;
let inGood = true;
console.log(isMarried);
console.log(inGood);
console.log(typeof isMarried);
console.log(typeof inGood);


let array = "this is not an array";
console.log(array)
array = [12, 23, 42];
console.log(array)

let details = ["John", 32, true];
console.log(details);

//Objects:

	let person = {
		firstName: "John",
		lastName: "Smith",
		age: 23,
		isMarried: false,
		contact: ["+639123456789","8123-4567"],
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	}

	console.log(person)

/*
	Constant Objects and Arrays
*/
	const anime = ["one piece", "one punch man", "your lie in April"];
	console.log(anime);
	anime[3] = 'kimetsu no yaiba';
	console.log(anime);

//const variables' value cannot be replaced
	/*const anime1 = ["one piece", "one punch man", "your lie in April"];
	console.log(anime1);
	anime1 = 'kimetsu no yaiba';
	console.log(anime1);*/

//In const variable array, you can add and modify the values of the elements

//Null
	//It is used to intentionally express the absence of a value in a variable declaration/intialization
	let spouse = null;
	console.log(spouse);

//Undefined
	//represent the state of a variable that has been declared but without an assigned value

let fullName;
console.log(fullName);





